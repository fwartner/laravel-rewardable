<?php

namespace DraperStudio\Rewardable\Traits;

trait Rewardable
{
    use Rankable;
    use Creditable;
    use Badgeable;
    use Transactionable;
}
