<?php

namespace DraperStudio\Rewardable\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use DraperStudio\Database\Models\Eloquent\Model;

class CreditType extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = ['build_from' => 'name'];

    public function credits()
    {
        return $this->hasMany(Credit::class);
    }
}
