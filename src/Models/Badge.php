<?php

namespace DraperStudio\Rewardable\Models;

use DraperStudio\Database\Models\Eloquent\Model;

class Badge extends Model
{
    protected $dates = ['awarded_at', 'revoked_at'];

    public function badgeable()
    {
        return $this->morphTo();
    }

    public function requirementType()
    {
        return $this->belongsTo(CreditType::class, 'requirement_type_id');
    }
}
