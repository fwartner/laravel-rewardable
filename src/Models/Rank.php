<?php

namespace DraperStudio\Rewardable\Models;

use DraperStudio\Database\Models\Eloquent\Model;

class Rank extends Model
{
    protected $dates = ['awarded_at', 'revoked_at'];

    public function rankable()
    {
        return $this->morphTo();
    }

    public function requirementType()
    {
        return $this->belongsTo(CreditType::class, 'requirement_type_id');
    }
}
